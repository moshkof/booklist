package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Book struct {
	ID     int    `json:"id"`
	Title  string `json:"title"`
	Author string `json:"author"`
	Year   string `json:"year"`
}

var books []Book

func main() {
	router := mux.NewRouter()

	books = append(books, Book{ID: 1, Title: "One", Author: "One", Year: "2011"},
		Book{ID: 2, Title: "Two", Author: "Two", Year: "2012"},
		Book{ID: 3, Title: "Three", Author: "Three", Year: "2013"},
		Book{ID: 4, Title: "Four", Author: "Four", Year: "2014"},
		Book{ID: 5, Title: "Five", Author: "Five", Year: "2015"})
	

	router.HandleFunc("/books", getBooks).Methods("GET")
	router.HandleFunc("/books/{id}", getBook).Methods("GET")
	router.HandleFunc("/books", addBook).Methods("POST")
	router.HandleFunc("/books", updateBook).Methods("PUT")
	router.HandleFunc("/books/{id}", removeBook).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8000", router))
}

func getBooks(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(books)
}

func getBook(w http.ResponseWriter, r *http.Request) {
	log.Println("Get one book")
}

func addBook(w http.ResponseWriter, r *http.Request) {
	log.Println("Adds one books")
}

func updateBook(w http.ResponseWriter, r *http.Request) {
	log.Println("Update a books")
}

func removeBook(w http.ResponseWriter, r *http.Request) {
	log.Println("Delete one books")
}
